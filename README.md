### Project Information

This console application allows many clients connect to one server and communicate via text messages.  
Many Clients - One Server Pattern.
Two projects Client and Server in one Solution.

By default one server starts then one client starts.  
To start many clients you need to start compiled exe file from debug/release folder.
