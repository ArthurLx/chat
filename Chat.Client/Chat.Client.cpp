#include <iostream>
#include <string>

#include <ws2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

SOCKET Connection;

void ClientHandler() 
{

	int client_id, callsign_size, m_size;
	std::string callsign_string;
	std::string output_msg;

	while (true) {
		int server_connected = recv(Connection, (char*)&m_size, sizeof(int), NULL);

		if (server_connected == SOCKET_ERROR) {
			std::cout << "SERVER_LOST" << std::endl;
			return;
		}

		char* message = new char[m_size + 1];
		message[m_size] = '\0';

		recv(Connection, message, m_size, NULL);

		recv(Connection, (char*)&client_id, sizeof(int), NULL);


		recv(Connection, (char*)&callsign_size, sizeof(int), NULL);
		char* callsign = new char[callsign_size + 1];
		callsign[callsign_size] = '\0';

		recv(Connection, callsign, callsign_size, NULL);

		callsign_string.assign(callsign);
		
		if (client_id == 0) { //Message from server
			output_msg = "(" + callsign_string + "): " + message;
		}
		else {
			output_msg = callsign_string + "(ID" + std::to_string(client_id) +  "): " + message;
		}

		std::cout << output_msg << std::endl;
		std::cout << "> ";

		delete[] message;
		delete[] callsign;
	}
}


int main(int argc, char* argv[]) 
{
	WSAData wsaData;		//0x202
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cout << "Error at WSAStartup: " << WSAGetLastError() << std::endl;
		return 1;
	}

	SOCKADDR_IN sock_address;
	InetPtonW(AF_INET, L"127.0.0.1", &sock_address.sin_addr.s_addr);
	sock_address.sin_port = htons(8050);
	sock_address.sin_family = AF_INET;

	Connection = socket(AF_INET, SOCK_STREAM, NULL);
	if (connect(Connection, (SOCKADDR*)&sock_address, sizeof(sock_address)) != 0) {
		std::cout << "Error at connection to server: " << WSAGetLastError() << std::endl;
		return 1;
	}
	std::cout << "Connected!\n";

	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandler, NULL, NULL, NULL);

	std::string msg1;
	while (true) {
		std::cout << "> ";
		std::getline(std::cin, msg1);
		int m_size = msg1.size();

		send(Connection, (char*)&m_size, sizeof(int), NULL);
		send(Connection, msg1.c_str(), m_size, NULL);

		Sleep(10);
	}

	return 0;
}