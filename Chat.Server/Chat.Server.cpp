#include <iostream>
#include <string>
#include <vector>

#include <ws2tcpip.h>


#pragma comment(lib, "ws2_32.lib")

SOCKET Connections[128];
std::vector<std::vector<std::string>> clientsConnectedData;

int Counter = 1;

std::string getCallSign(int client_id)
{
	std::string callsign;
	switch (client_id) {
	case 0:
		callsign = "Server";
		break;
	case 1:
		callsign = "Bro";
		break;
	case 2:
		callsign = "Guy";
		break;
	case 3:
		callsign = "Dude";
		break;
	case 4:
		callsign = "Bob";
		break;
	default:
		callsign = "randomdude";
		break;
	}
	return callsign;
}

void ClientHandler(int index)
{
	int m_size;
	std::string callsign_msg;

	std::vector<std::string> rawData = clientsConnectedData[index - 1];

	while (true) {
		int client_connected = recv(Connections[index], (char*)&m_size, sizeof(int), NULL);

		if (client_connected == SOCKET_ERROR) {
			std::cout << "CLIENT " << rawData[1] // host_name
								<< " (" << rawData[2] // host_ip
								<< ") DISCONNECTED from port " << rawData[3] // service (i.e. port number)
								<< std::endl;
			return;
		}
		char* message = new char[m_size + 1];
		message[m_size] = '\0';

		recv(Connections[index], message, m_size, NULL);

		for (int i = 1; i < Counter; i++) {
			if (i == index) {
				continue;
			}

			send(Connections[i], (char*)&m_size, sizeof(int), NULL);
			send(Connections[i], message, m_size, NULL);
			send(Connections[i], (char*)&index, sizeof(int), NULL);

			callsign_msg = getCallSign(index);
			int callsign_msg_size = callsign_msg.size();

			send(Connections[i], (char*)&callsign_msg_size, sizeof(int), NULL);
			send(Connections[i], callsign_msg.c_str(), callsign_msg_size, NULL);
		}

		delete[] message;
	}
}

int main(int argc, char* argv[]) 
{

	WSAData wsaData;		//0x202
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		std::cerr << "Error at WSAStartup: " << WSAGetLastError() << std::endl;
		return 1;
	}

	SOCKADDR_IN serverSocket_info;
	InetPtonW(AF_INET, L"127.0.0.1", &serverSocket_info.sin_addr.s_addr);
	serverSocket_info.sin_port = htons(8050);
	serverSocket_info.sin_family = AF_INET;


	SOCKET listening = socket(AF_INET, SOCK_STREAM, NULL);
	bind(listening, (SOCKADDR*)&serverSocket_info, sizeof(serverSocket_info));
	listen(listening, SOMAXCONN);

	SOCKET newConnection;

	SOCKADDR_IN clientSocket_info;
	int clientSize = sizeof(clientSocket_info);

	char host_name[NI_MAXHOST]; // Client's remote name
	char host_ip[NI_MAXSERV]; // Client's ip address
	char service[NI_MAXSERV]; // Service (i.e. port) the client is connect on

	for (int i = 1; i < 128; i++) {

		ZeroMemory(&clientSocket_info, clientSize);
		newConnection = accept(listening, (SOCKADDR*)&clientSocket_info, &clientSize);

		if (newConnection == INVALID_SOCKET) {
			std::cerr << "Error_at_client_connection: " << WSAGetLastError() << std::endl;
			return 1;
		}

		ZeroMemory(host_name, NI_MAXHOST);
		ZeroMemory(host_ip, NI_MAXSERV);
		ZeroMemory(service, NI_MAXSERV);

		inet_ntop(AF_INET, &clientSocket_info.sin_addr, host_ip, NI_MAXSERV);
		getnameinfo((sockaddr*)&clientSocket_info, sizeof(clientSocket_info), host_name, NI_MAXHOST, service, NI_MAXSERV, 0);

		std::cout << "Client " << host_name << " (" << host_ip << ") connected on port "<< service << std::endl;
		std::string greeting_msg = "Hi there, client! Your ID is set to " + std::to_string(i)
									+ "\nAnd your callsign is " + getCallSign(i) + ". Enjoy conversation.";
		int gm_size = greeting_msg.size();

		send(newConnection, (char*)&gm_size, sizeof(int), NULL);
		send(newConnection, greeting_msg.c_str(), gm_size, NULL);

		int server_id = 0;
		send(newConnection, (char*)&server_id, sizeof(int), NULL);

		std::string callsign_msg = getCallSign(0);
		int callsign_msg_size = callsign_msg.size();
		send(newConnection, (char*)&callsign_msg_size, sizeof(int), NULL);
		send(newConnection, callsign_msg.c_str(), callsign_msg_size, NULL);


		Connections[i] = newConnection;
		Counter++;
		
		std::vector<std::string> params;

		params.push_back(std::to_string(i));
		params.push_back(host_name);
		params.push_back(host_ip);
		params.push_back(service);

		clientsConnectedData.push_back(params);

		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandler, (LPVOID)(i), NULL, NULL);

	}

	return 0;
}